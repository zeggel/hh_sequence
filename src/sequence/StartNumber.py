# encoding: utf-8
import re
'''
Класс используется для поиска минимального числа последовательности чисел которой соответсвует искомая подпоследователность
и поиска позиции числа в бесконечной подпоследовательности
Для искомой подпоследовательности символов вычисляются соответствующие последовательности чисел,
из которых выбирается минимальное начальное число
'''
class StartNumber:
    '''
    Конструктор.
    @param number: Искомая подпоследовательность
    '''
    def __init__(self, number):
        if int(number) == 0:
            raise ValueError('Invalid number ' + number)
        self.number = number
        self.remainder = number[:]
        self.initFactorAndShift()
        self.row = []
        self.prefix = ''
    
    '''
    Инициализация порядка числа и сдвига чисел в последовательности
    '''
    def initFactorAndShift(self):
        factor = 1
        for digit in list(self.number):
            if digit != '0':
                break
            factor += 1
        self.setFactor(factor)
        self.setShift(factor - 1)
    
    '''
    Устанавливает порядок числа в последовательности
    '''
    def setFactor(self, factor):
        if (factor > len(self.number)):
            raise ValueError('Factor ' + str(factor) + ' should be less than or equal number length ' + str(len(self.number)))
        self.factor = factor
        #self.shift = 0
    
    '''
    Устанавливает сдвиг в последовательности
    '''
    def setShift(self, shift):
        if shift >= self.factor:
            raise ValueError('Shift ' + str(shift) + ' should be less than factor ' + str(self.factor))
        self.shift = shift
        self.prefix = self.number[:shift]
        self.remainder = self.number[shift:]
    
    '''
    Делит self.number на последовательность чисел порядка self.factor и добавляет их в self.row,
    если эти числа идут не по порядку возвращает False.
    '''
    def split(self):
        prev = None
        while len(self.remainder) >= self.factor:
            part = self.remainder[:self.factor]
            if (part.startswith('0')):
                return False
            sequence_number = int(part)
            if prev is not None and prev + 1 != sequence_number:
                return False
            else:
                prev = sequence_number
            self.row.append(sequence_number)
            self.remainder = self.remainder[self.factor:]
            if sequence_number + 1 == 10 ** self.factor:
                self.factor += 1
        return True
    
    '''
    Вычиляет начальное число последовательности, которая начинается с числа заканчивающегося на self.prefix,
    продолжающаяся числами self.row и заканчивающаяся числом начинающемся с self.remainder
    '''
    def calcValue(self):
        if not self.split():
            return False
        if self.row:
            if self.remainder and not str(self.row[-1] + 1).startswith(self.remainder):
                return False
            if self.prefix:
                if str(self.row[0] - 1).endswith(self.prefix):
                    return self.row[0] - 1
                else:
                    return False
            return self.row[0]
        else:
            if self.remainder.startswith('0'):
                return False
            if self.prefix and self.remainder:
                d = len(self.number) - len(self.remainder)
                if int(self.prefix) != 0 and str(int(self.remainder.ljust(d, '0')) - 1).endswith(self.prefix):
                    return int(self.remainder.ljust(d, '0')) - 1
                elif re.match('^9+$', self.prefix):
                    return int(str(int(self.remainder)-1) + self.prefix)
                else:
                    return int(self.remainder + self.prefix)
            if self.prefix:
                return int(self.prefix)
            if self.remainder:
                return int(self.remainder)
        
        return False
    
    '''
    Вычисляет минимальное начальное число из возможных последовательностей чисел, соответствующих искомой
    подпоследовательности self.number
    '''
    def calcMinValue(self):
        if len(self.number) > 1 and self.number[0] != '0' and int(self.number[1:]) == 0:
            return int(self.number)
        len_ = len(self.number)
        max_factor = len_
        if self.shift == 0:
            result_value = int(self.number)
        else:
            result_value = None
        for factor in range(self.factor, max_factor + 1):
            self.setFactor(factor)
            for shift in range(self.shift, factor):
                self.setShift(shift)
                self.row = []
                value = self.calcValue()
                if value:
                    if result_value is None or value < result_value:
                        result_value = value

        return result_value
    
    '''
    Вычисляет сдвиг искомой подпоследовательности self.number относительно указанного числа
    @param startNumber: Число, относительно которого вычисляем сдвиг
    '''
    def calcResultShift(self, startNumber):
        number = startNumber
        seq = ''
        while len(seq) <= len(self.number):
            seq += str(number)
            shift = seq.find(self.number)
            if shift >= 0:
                return shift
            number += 1
        raise ValueError('Invalid start number ' + str(startNumber) + ' for ' + self.number)
    
    '''
    Вычислияет позицию указанного числа в бесконечной последовательности
    '''
    @classmethod
    def calcNumPosition(cls, number):
        if number <= 10:
            return number
        length_ = len(str(number))
        #result = number % 10 * length_
        result = (number - 10 ** (length_ - 1)) * length_
        for factor in range(length_ - 1):
            result += 9 * (10 ** factor) * (factor + 1)
            
        return result + 1
    
    
        