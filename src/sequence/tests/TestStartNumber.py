import unittest
from sequence.StartNumber import StartNumber

class TestStartNumber(unittest.TestCase):
        
    def testSplitShouldHasRowWithNumberIfNumberLengthEqualsfactor(self):
        start_number = StartNumber('23')
        start_number.setFactor(2)
        start_number.split()
        self.assertEqual([23], start_number.row)
        
    def testSplitShouldReturnFalseIfAnyPartStartsWithZero(self):
        start_number = StartNumber('203')
        actual = start_number.split()
        self.assertEqual(False, actual)
    
    def testSplitShouldReturnFalseIfPartsIsNotSequence(self):
        start_number = StartNumber('1256')
        actual = start_number.split()
        self.assertEqual(False, actual)
    
    def testSplitShouldFillRowIfNumberLengthGretherThanFactor(self):
        start_number = StartNumber('123456')
        start_number.split()
        self.assertEqual([1,2,3,4,5,6], start_number.row)
        self.assertEqual('', start_number.remainder)
        
    def testSplitShouldIncreaseFactorIfSequenceNumberIsMax(self):
        start_number = StartNumber('8910113')
        start_number.split()
        self.assertEqual([8,9,10,11], start_number.row)
        self.assertEqual('3', start_number.remainder)

    def testCalcValue(self):
        params = (
            ('1', 1, 0, 1),
            ('10', 1, 0, False),
            ('10', 2, 0, 10),
            ('7891011', 1, 0, 7),
            ('72', 2, 1, 27),
            ('22324', 2, 1, 22),
        )
        for number, factor, shift, expected in params:
            start_number = StartNumber(number)
            start_number.setFactor(factor)
            start_number.setShift(shift)
            actual = start_number.calcValue()
            
            self.assertEqual(expected, actual, 'Expected is %s, actual is %s' % (str(expected), str(actual)))
            
    def testCalcMinValue(self):
        params = (
            ('1', 1),
            ('10', 10),
            ('12', 1),
            ('21', 12),
            ('23', 2),
            ('92', 19),
            ('99', 89),
            ('100', 100),
            ('102', 102),
            ('110', 101),
            ('111', 11),
            ('112', 11),
            ('01', 10),
            ('001', 100),
            ('02', 20),
            ('990', 89),
            ('992', 199),
            ('60378', 37860)
        )
        for number, expected in params:
            start_number = StartNumber(number)
            actual = start_number.calcMinValue()
            
            self.assertEqual(expected, actual, 'For %s expected is %s, actual is %s' % (str(number), str(expected), str(actual)))

    def testCalcNumPosition(self):
        params = (
            (1, 1),
            (2, 2),
            (10, 10),
            (11, 12),
            (12, 14),
            (19, 28),
            (20, 30),
            (100, 190),
            (101, 193),
            (102, 196)
        )
        for number, expected in params:
            actual = StartNumber.calcNumPosition(number)
            
            self.assertEqual(expected, actual, 'Expected is %s, actual is %s' % (str(expected), str(actual)))


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()