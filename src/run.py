#!/usr/local/bin/python2.7
# encoding: utf-8
from sys import stdin
from sequence.StartNumber import StartNumber

for line in stdin.readlines():    
    subseq = line.strip()
    if not subseq.isdigit():
        raise ValueError('Invalid value ' + subseq)
    
    if int(subseq) == 0:
        minNumber = int('1' + subseq)
        position = StartNumber.calcNumPosition(minNumber)
        print position + 1
    else:
        startNumber = StartNumber(subseq)
        minNumber = startNumber.calcMinValue()
        position = StartNumber.calcNumPosition(minNumber)
        print position + startNumber.calcResultShift(minNumber)
    